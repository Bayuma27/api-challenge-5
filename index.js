const express = require("express");
const app = express();
const port = 8000;
const {join} = require("path");
const userRouter = require("./user/route");
const path = require("path");

app.use(express.json());
app.use(express.static('public'));

app.get("/", (req, res) => {
  res.sendFile("index.html");
});

app.get("/game", (req, res) => {
  res.sendFile(join(__dirname, "public", "game", "index.html"));
});

app.use("/", userRouter);

app.get("/ping", (req, res) => {
  res.json("pong");});

app.listen(8000, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

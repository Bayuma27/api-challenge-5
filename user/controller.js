const userModel = require('./model');

class UserController {
    getAlluser = (req, res) => {
    const allUsers = userModel.getAllusers();
    return res.json(allUsers);
    };

    registrasiUser = (req, res) => {
        const dataRequest = req.body;
        if (dataRequest.username === undefined || dataRequest.username === "") {
          res.statusCode = 400;
          return res.json({ message: "Username is Invalid" });
        }
        if (dataRequest.email === undefined || dataRequest.email === "") {
          res.statusCode = 400;
          return res.json({ message: "Email is Invalid" });
        }
        if (dataRequest.password === undefined || dataRequest.password === "") {
          res.statusCode = 400;
          return res.json({ message: "Password is Invalid" });
        }
      
        const existData = userModel.isUserRegistered(dataRequest);
      
        if (existData) {
          return res.json({ message: USERNAMENOTEXIS });
        }
      
        //Menambahkan data baru
        userModel.recordNewData(dataRequest);
        return res.json({ message: "New User Is Recorded"});
      };
      //Login User
      loginUser = (req, res) => {
        const { username, email, password } = req.body;
        const dataLogin = userModel.userLogin(username, email, password)
      
      if (dataLogin) {
        return res.json(dataLogin);
      } else {
        return res.json({ message: "Invalid credential" });
      }
      };
}

module.exports = new UserController();
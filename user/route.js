const express = require('express');
const userRouter = express.Router();
const userController = require('./controller');

userRouter.get("/user", userController.getAlluser);

userRouter.post("/registrasi", userController.registrasiUser);

userRouter.post("/login", userController.loginUser);
  
module.exports = userRouter;